import * as fs from "fs";
import { dirname } from "path";
import { fileURLToPath } from "url";
import lodashPackage from "lodash";
const { _ } = lodashPackage;
import * as dotenv from "dotenv";

import { abi } from "./assets/abi.js";
import { web3, redisClient } from "./init.js";

dotenv.config();
const __dirname = dirname(fileURLToPath(import.meta.url));

// https://community.infura.io/t/web3-js-how-to-track-erc-20-token-transfers-specific-address-token/5571
export async function subscribeOnLogs() {
  cl(`Subscribe on logs`);
  let options = {
    topics: [web3.utils.sha3("Transfer(address,address,uint256)")],
  };
  let subscription = web3.eth.subscribe("logs", options);
  subscription.on("error", (err) => {
    throw err;
  });
  subscription.on("connected", (nr) =>
    cl("Subscription on ERC-20 started with ID " + nr)
  );
  subscription.on("data", async (event) => {
    let txTime = Date.now();
    if (event.topics.length === 3) {
      let transaction = await decodeTransaction(event);
      let subscribedWallets = await getWalletsDB();
      for (let walletAddress of subscribedWallets) {
        if (
          transaction.from === walletAddress ||
          transaction.to === walletAddress
        ) {
          await saveTx(walletAddress, event.address, transaction, txTime);
        }
      }
    }
  });
}

// export async function scanWallet(walletAddress) {

//   !EVENT EMITTER HERE MAYBE

//   const tokens = await fn.getListERC20();
//   let tokensCount = 0;
//   for (let tokenData of tokens) {
//     const { address } = tokenData;
//     let tokenBalanceData;
//     try {
//       tokenBalanceData = await fn.getTokenBalanceETH(walletAddress, address);
//     } catch (err) {
//       fn.consoleWarn(
//         `Could not retrieve balance for ${walletAddress}, ${address}, few attemptions were done. Skip token ${address}`
//       );
//       continue;
//     }
//     let { balance, time } = tokenBalanceData;
//     if (balance && balance > 0) {
//       let symbol = await fn.getSymbolName(address);
//       if (!symbol) {
//         fn.consoleWarn(
//           `Skip treating token ${address}, balance=${balance}, because of unable to get its symbol.`
//         );
//         continue;
//       }
//       let output = tokensCount === 0 ? ', "balances":{' : "";
//       output += tokensCount === 0 ? "" : ", ";
//       output += `"${symbol}": "${balance}"`;
//       resp.write(output);
//       fn.cl(`Sent chunk to client: ${output}`);
//       await fn.saveTokenBalanceDB(walletAddress, symbol, balance, time);
//       await fn.saveSymbolNameDB(address, symbol);
//       tokensCount += 1;
//     }
//   }
// }

export async function getTotalBalanceETH(walletAddress) {
  try {
    let balance = await web3.eth.getBalance(walletAddress);
    return balance;
  } catch (err) {
    throw err;
  }
}

// Different operators (Infura, Alchemy etc.) suggest
// different conditions for allowed RPS value.
// When few concurrent requests are occured,
// then an operator may return error like "Exceeded RPS limit".
// As simple solution, this function implements retries of balanceOf()
// with delay between them, if current request is unsuccessfull.
//
// Output example:
// {balance: "6543134646646", time: 1565487977}
export async function getTokenBalanceETH(walletAddress, address) {
  let balance = -1;
  let time = 0;
  let maxAttemptions = 20;
  let attemption = 0;
  let delayMs = 300;
  while (balance < 0 && attemption < maxAttemptions) {
    try {
      const contract = new web3.eth.Contract(abi, address);
      balance = await contract.methods.balanceOf(walletAddress).call();
      time = Date.now();
    } catch (err) {
      attemption += 1;
      await delay(delayMs);
    }
  }
  if (balance < 0) {
    throw new Error("Cannot retrieve balance.");
  }
  return { balance, time };
}

// Output example:
// {balance: "6543134646646", time: 1565487977}
export async function getTokenBalanceDB(walletAddress, symbol) {
  let balanceRaw = await redisClient.hGet(walletAddress + ":balance", symbol);
  if (_.isEmpty(balanceRaw)) {
    balanceRaw = null;
  }
  return JSON.parse(balanceRaw);
}

// Return balances for walletAddress from internal database
// Saved transactions for walletAddress are *not* included
//
// Output example:
// {
//   USDT: {balance: 6543134646646n, time: 1565487977},
//   USDC: {balance: 13215467968465n, time: 1565484587}
// }
export async function getTokensBalancesDB(walletAddress) {
  let cachedTokensRaw = await redisClient.hGetAll(walletAddress + ":balance");
  cachedTokensRaw = _.isEmpty(cachedTokensRaw) ? {} : cachedTokensRaw;
  let cachedTokens = {};
  for (let symbol in cachedTokensRaw) {
    let { balance, time } = JSON.parse(cachedTokensRaw[symbol]);
    cachedTokens[symbol] = { balance: BigInt(balance), time };
  }
  return cachedTokens;
}

// Return balances for walletAddress from internal database
// Transactions for walletAddress *are* included
// (i.e. are summed with respective balances)
//
// Output example:
// {
//   USDT: 6543134646646n,
//   USDC: 13215467968465n
// }
export async function getWalletBalancesDB(walletAddress) {
  let balancesData = await getTokensBalancesDB(walletAddress);
  let txData = await getGroupedTxDB(walletAddress);
  for (let { symbol, value } of txData.deltasNewer) {
    balancesData[symbol] = (balancesData[symbol] || 0n) + value;
  }
  let balances = {};
  for (let symbol in balancesData) {
    balances[symbol] = balancesData[symbol].balance;
  }
  return balances;
}

// Return transactions from internal DB that *are newer* than respective balance
// ("respective" by symbol, like tx for USDT and balance for USDT too).
//
// Output example:
// {
//   newer: {
//     "3546545465r6465": {
//       address: "0xJkjd465sd6f45s6d48sD05",
//       transaction: { from: ... },
//       time: 6548798465,
//     },
//     "3546549875r6465": {
//       address: "0xJkjd465dfgb466d4fsL08",
//       transaction: { from: ... },
//       time: 6548778265,
//     },
//   },
//   older: {
//     "9879797465r6465": {
//       address: "0xJkjd465sd4513s6d4fkn04",
//       transaction: { from: ... },
//       time: 6548798465,
//     },
//     "9432139875r6465": {
//       address: "0xJkjd465874gHj66d4fsD01",
//       transaction: { from: ... },
//       time: 6548778265,
//     },
//   },
// };
export async function getTxNewerOlderDB(walletAddress) {
  let txDataRaw = await getTxDB(walletAddress);
  let txData = {};
  for (let txId in txDataRaw) {
    txData[txId] = JSON.parse(txDataRaw[txId]);
  }
  let txDataExtended = {};
  for (let txId in txData) {
    txDataExtended[txId] = {
      ...txData[txId],
      symbol: await getSymbolName(txData[txId].address),
    };
  }
  let txSeparated = {
    newer: {},
    older: {},
  };
  let balances = await getTokensBalancesDB(walletAddress);
  Object.keys(txDataExtended).forEach((txId) => {
    if (_isTxNewer(txDataExtended[txId], balances)) {
      txSeparated.newer[txId] = txDataExtended[txId];
    } else {
      txSeparated.older[txId] = txDataExtended[txId];
    }
  });
  return txSeparated;
}

function _isTxNewer(tx, balances) {
  let { symbol, time } = tx;
  return (
    typeof balances[symbol] === "undefined" || balances[symbol].time < time
  );
}

export async function getWalletsDB() {
  let walletsRaw = await redisClient.get("wallets");
  return _.isEmpty(walletsRaw) ? [] : JSON.parse(walletsRaw);
}

export async function getTxDB(walletAddress) {
  let txData = await redisClient.hGetAll(walletAddress + ":transaction");
  return _.isEmpty(txData) ? {} : txData;
}

// Returns artificial object with aim to make
// treating transactions easier on a higher level.
//  `deltasNewer` stores generalized data about transaction
// that are newer than respective balances (respective by symbols).
// It needs for easy summ balances and new transactions.
//  `idsAll` stores IDs of _all_ transactions (both older and newer
// than the respective balances). It need for easy deletion
// these transactions from an internal DB.
//
// Output example:
// {
//   deltasNewer: [{ symbol: "USDT", value: -152032654654 }, {symbol: "USDC": value: 6549843213}],
//   idsAll:["65465454654654r321321", "321332132r646546", "4654132132134r7987984"]
// }
export async function getGroupedTxDB(walletAddress) {
  let output = {
    deltasNewer: [],
    idsAll: [],
  };
  let trData = await getTxNewerOlderDB(walletAddress);
  let deltasNewer = [];
  for (let ms in trData.newer) {
    let { transaction, symbol } = trData.newer[ms];
    output.idsAll.push(ms);
    deltasNewer[symbol] =
      (deltasNewer[symbol] || BigInt(0)) +
      signValue(transaction, walletAddress);
  }
  for (let symbol in deltasNewer) {
    output.deltasNewer.push({ symbol, value: deltasNewer[symbol] });
  }
  for (let ms in trData.older) {
    output.idsAll.push(ms);
  }
  return output;
}

export async function removeTxDB(walletAddress, ms) {
  await redisClient.hDel(walletAddress + ":transaction", ms);
  clAsync(`Transaction ${walletAddress + ":transaction"}, ${ms} was removed`);
}

export async function getSymbolName(address) {
  let symbol = await getSymbolNameDB(address);
  if (!symbol) {
    try {
      symbol = await getSymbolNameETH(address);
    } catch (e) {
      symbol = null;
      consoleWarn(`Unable to get symbol by ${address}.`, e);
    }
  }
  return symbol;
}

export async function getSymbolNameDB(address) {
  let sn = await redisClient.get(address + ":symbol");
  return _.isEmpty(sn) ? null : sn;
}

export async function saveSymbolNameDB(address, symbol) {
  await redisClient.set(address + ":symbol", symbol);
  clAsync(`Symbol '${symbol}' was saved to DB`);
}

export async function getSymbolNameETH(address) {
  try {
    const contract = new web3.eth.Contract(abi, address);
    let sn = await contract.methods.symbol().call();
    return sn;
  } catch (err) {
    consoleWarn(`Unable to get symbol for ${address}`, err);
    return null;
  }
}

export async function saveTokenBalanceDB(walletAddress, symbol, balance, time) {
  await redisClient.hSet(
    walletAddress + ":balance",
    symbol,
    JSON.stringify({
      balance: balance.toString(),
      time,
    })
  );
  clAsync(`Save balance: ${walletAddress}, ${symbol}, ${balance.toString()}`);
}

export async function saveTx(walletAddress, address, transaction, time) {
  await redisClient.hSet(
    walletAddress + ":transaction",
    generateId(),
    JSON.stringify({ address, transaction, time }, stringifyHelper)
  );
  //for debug:
  clAsync("Save transaction: " + JSON.stringify(transaction));
}

export async function getListERC20() {
  const tokensAll = JSON.parse(
    await readFile(__dirname + "/assets/tokens_all.json")
  );
  let result = tokensAll.reduce((reducedERC20tokens, token) => {
    if (
      typeof token.platforms !== "undefined" &&
      typeof token.platforms.ethereum !== "undefined" &&
      token.platforms.ethereum.length > 1
    ) {
      reducedERC20tokens.push({
        address: token.platforms.ethereum,
        symbol: token.symbol.toUpperCase(),
      });
    }
    return reducedERC20tokens;
  }, []);
  // result = result.slice(0, 100); //for debug
  return result;
}

export async function readFile(path) {
  let content = await fs.promises.readFile(path, { encoding: "utf8" });
  return content;
}

export async function appendFile(path, str) {
  await fs.promises.appendFile(path, str, { encoding: "utf8" });
}

export async function subscribeAddress(walletAddress) {
  let wallets = JSON.parse((await redisClient.get("wallets")) || "[]");
  if (wallets.indexOf(walletAddress) < 0) {
    clAsync(`Subscribe wallet '${walletAddress}'`);
    wallets.push(walletAddress);
    redisClient.set("wallets", JSON.stringify(wallets));
  }
}

function signValue(transaction, walletAddress) {
  let signedValue = BigInt(transaction.value);
  if (transaction.from === walletAddress) {
    signedValue *= -1n;
  }
  return signedValue;
}

export async function saveWalletLabel(walletAddress, label) {
  await redisClient.set(walletAddress + ":label", label);
}

export async function getWalletLabel(walletAddress) {
  return await redisClient.get(walletAddress + ":label");
}

export async function removeWalletLabel(walletAddress) {
  await redisClient.del(walletAddress + ":label");
}

async function decodeTransaction(event) {
  return new Promise((resolve) => {
    resolve(
      web3.eth.abi.decodeLog(
        [
          {
            type: "address",
            name: "from",
            indexed: true,
          },
          {
            type: "address",
            name: "to",
            indexed: true,
          },
          {
            type: "uint256",
            name: "value",
            indexed: false,
          },
        ],
        event.data,
        [event.topics[1], event.topics[2], event.topics[3]]
      )
    );
  });
}

export async function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("");
    }, ms);
  });
}

export function consoleWarn(str, err = { name: "", message: "" }) {
  let ename = err.name || "";
  let emessage = err.message || "";
  let { locale, tzOptions } = _getDateLocalOpts();
  console.warn(
    new Date().toLocaleString(locale, tzOptions) + " " + str,
    ename,
    emessage
  );
}

export function consoleLog(str) {
  if (process.env.LOGGING === "false") {
    return;
  }
  console.log(getDateTime() + " " + str);
}

export function consoleLogAsync(str) {
  setTimeout(() => {
    consoleLog(str);
  }, 0);
}

export const cl = consoleLog;

export const clAsync = consoleLogAsync;

export function generateId() {
  return Date.now() + "r" + getRandomArbitrary(10000000, 99999999);
}

export function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

export function getDateTime() {
  let { locale, tzOptions } = _getDateLocalOpts();
  return new Date().toLocaleString(locale, tzOptions);
}

// It is used in JSON.stringify() as second parameter
// https://github.com/GoogleChromeLabs/jsbi/issues/30
export function stringifyHelper(key, value) {
  return typeof value === "bigint" ? value.toString() : value;
}

// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
// https://gist.github.com/typpo/b2b828a35e683b9bf8db91b5404f1bd1
// https://docs.oracle.com/cd/E19455-01/806-0169/overview-7/index.html
function _getDateLocalOpts() {
  return {
    locale: "en-CA",
    tzOptions: {
      timeZone: "Europe/Kiev",
      year: "numeric",
      month: "numeric",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
      fractionalSecondDigits: 3,
      hour12: false,
    },
  };
}
