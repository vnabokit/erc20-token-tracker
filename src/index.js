import { createServer, IncomingMessage, ServerResponse } from "http";
import lodashPackage from "lodash";
const { _ } = lodashPackage;

import * as fn from "./functions.js";

const server = createServer(
  // async (req: IncomingMessage, resp: ServerResponse) => {
  async (req, resp) => {
    // const urlString: string = req.url || "";
    const urlString = req.url || "";
    let parsedEndpointToken = urlString.match(/^\/0x(.+)$/);

    if (parsedEndpointToken) {
      let walletAddress = "0x" + parsedEndpointToken[1];
      fn.cl(`Wallet address is: ${walletAddress}`);
      let totalBalance;
      try {
        totalBalance = await fn.getTotalBalanceETH(walletAddress);
        fn.cl(`Total balance: ${totalBalance}`);
      } catch (e) {
        fn.consoleWarn("Error while getting totalBalance.", e);
        resp.setHeader("Content-Type", "text/html");
        resp.statusCode = 404;
        resp.write(`Error while retrieving data for '${walletAddress}'`);
        resp.end();
        return;
      }

      let walletBalances = await fn.getWalletBalancesDB(walletAddress);
      const LABEL = "caching...";
      let isWalletCachingNow =
        (await fn.getWalletLabel(walletAddress)) === LABEL;
      if (_.isEmpty(walletBalances) && !isWalletCachingNow) {
        await fn.subscribeAddress(walletAddress); //subscribe on a wallet firts of all for do not missing transactions
        await fn.saveWalletLabel(walletAddress, LABEL);
        fn.cl(`'${walletAddress}' is not cached. Scan all tokens.`);
        resp.setHeader("Content-Type", "application/json");
        resp.statusCode = 200;
        resp.write("{");
        resp.write(`"address": "${walletAddress}",`);
        resp.write(
          `"message": "WARNING: Data for this address is not cached yet. Querying may take few minutes (10-15 minutes), please wait. Next time querying for '${walletAddress}' will be much faster.",`
        );
        resp.write(` "totalBalance": "${totalBalance}"`);

        const tokens = await fn.getListERC20();
        let tokensCount = 0;
        for (let tokenData of tokens) {
          const { address } = tokenData;
          let tokenBalanceData;
          try {
            tokenBalanceData = await fn.getTokenBalanceETH(
              walletAddress,
              address
            );
          } catch (err) {
            fn.consoleWarn(
              `Could not retrieve balance for ${walletAddress}, ${address}, few attemptions were done. Skip token ${address}`
            );
            continue;
          }
          let { balance, time } = tokenBalanceData;
          if (balance && balance > 0) {
            let symbol = await fn.getSymbolName(address);
            if (!symbol) {
              fn.consoleWarn(
                `Skip treating token ${address}, balance=${balance}, because of unable to get its symbol.`
              );
              continue;
            }
            let output = tokensCount === 0 ? ', "balances":{' : "";
            output += tokensCount === 0 ? "" : ", ";
            output += `"${symbol}": "${balance}"`;
            resp.write(output);
            fn.cl(`Sent chunk to client: ${output}`);
            await fn.saveTokenBalanceDB(walletAddress, symbol, balance, time);
            await fn.saveSymbolNameDB(address, symbol);
            tokensCount += 1;
          }
        }
        resp.end(tokensCount === 0 ? "}" : "} }");
        fn.cl("Finished.");
        await fn.removeWalletLabel(walletAddress);
      } else {
        resp.setHeader("Content-Type", "application/json");
        resp.statusCode = 200;
        resp.end(
          JSON.stringify(
            {
              address: walletAddress,
              message: isWalletCachingNow
                ? "WARNING: This address is being caching now. This process needs some time. New tokens and their balances may be found when the caching is finished. Please wait few minutes and refresh the page. Server time: '" +
                  fn.getDateTime() +
                  "'"
                : "Data is retrieved succesfully.",
              totalBalance,
              balances: walletBalances,
            },
            fn.stringifyHelper
          )
        );
        return;
      }
    } else {
      resp.setHeader("Content-Type", "text/html");
      resp.statusCode = 404;
      resp.end(
        `Endpoint not found. The expected URL is: ${process.env.SERVER_HOST}:${process.env.SERVER_port}/<WALLET ADDRESS ERC-20 COMPATIBLE>`
      );
    }
  }
);

server.listen(process.env.SERVER_PORT, () => {
  console.log(`Server listens at ${process.env.SERVER_PORT}`);
});
