import Web3 from "web3";
import { createClient } from "redis";
import * as dotenv from "dotenv";

dotenv.config();
export const web3 = new Web3(
  new Web3.providers.WebsocketProvider(process.env.ETHERUM_NETWORK_URL)
);

export const redisClient = createClient({
  url: process.env.REDIS_URL,
});
redisClient.on("error", (err) => {
  console.log("Redis Client Error", err);
});
redisClient.on("connect", () => {
  console.log("Redis DB is connected");
});
redisClient.connect();
