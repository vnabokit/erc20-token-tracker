export type TokenInfo = {
  address: string;
  code: string;
};

export type AccountBalances = {
  totalBalance: number;
  tokenBalances: { code: string; balance: number }[];
};
