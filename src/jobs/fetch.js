import * as dotenv from "dotenv";
import * as url from "url";
import { get } from "http";
import { CronJob } from "cron";

import * as fn from "../functions.js";

dotenv.config();
const __dirname = url.fileURLToPath(new URL(".", import.meta.url));
const { SERVER_HOST, SERVER_PORT, ETH_ADDRESS } = process.env;

function main() {
  fn.cl("Task is started.");
  get(`${SERVER_HOST}:${SERVER_PORT}/${ETH_ADDRESS}`, (resp) => {
    let data = "";
    if (resp.statusCode != 200 && resp.statusCode != 201) {
      data = "ERROR (status code = " + resp.statusCode + ") ";
      resp.on("data", (chunk) => {
        data += chunk;
      });
      // throw new Error(
      //   `Unexpected response. Code: ${resp.statusCode}, data: ${errorData}`
      // );
    } else {
      resp.on("data", (chunk) => {
        data += chunk;
      });

      resp.on("end", async () => {
        // let walletData = JSON.parse(data);
        // if (walletData === null) {
        //   throw new Error(`Could not parse data correctly: ${data}`);
        // }
        let output = {
          timeFetched: new Date().toUTCString(),
          data,
        };
        await fn.appendFile(
          __dirname + "/fetched.txt",
          JSON.stringify(output) + "\n\n"
        );
      });
    }
  }).on("error", (err) => {
    throw err;
  });
  fn.cl("Task is finished.");
}

// https://stackoverflow.com/questions/57748085/node-cron-wait-for-job-finished-to-execute-the-next-one
var taskRunning = false;
const job = new CronJob({
  cronTime: "*/1 * * * *",
  onTick: () => {
    if (taskRunning) {
      return;
    }
    taskRunning = true;
    try {
      main();
    } catch (err) {
      console.warn("Error while executing the job:", err);
    }
    taskRunning = false;
  },
  start: true,
  timeZone: "Europe/Kiev",
});
