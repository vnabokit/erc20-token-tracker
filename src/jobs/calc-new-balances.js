import { CronJob } from "cron";
import * as fn from "../functions.js";

async function main() {
  fn.clAsync("Job is started.");
  let wallets = await fn.getWalletsDB();
  fn.cl(`Subscribed wallets: ` + JSON.stringify(wallets));
  for (let walletAddress of wallets) {
    fn.cl("Wallet " + walletAddress);
    let trGrouped = await fn.getGroupedTxDB(walletAddress);
    fn.clAsync(
      `Grouped transactions for ${walletAddress}: ${JSON.stringify(
        trGrouped,
        fn.stringifyHelper
      )}`
    );
    for (let { symbol, value } of trGrouped.deltasNewer) {
      let balanceData = await fn.getTokenBalanceDB(walletAddress, symbol);
      let oldBalance = balanceData ? BigInt(balanceData.balance) : 0n;
      await fn.saveTokenBalanceDB(
        walletAddress,
        symbol,
        oldBalance + BigInt(value),
        Date.now()
      );
    }
    for (let trId of trGrouped.idsAll) {
      await fn.removeTxDB(walletAddress, trId);
    }
  }
  fn.clAsync("Job is started.");
}

var taskRunning = false;

const job = new CronJob({
  cronTime: "*/1 * * * *",
  onTick: async () => {
    if (taskRunning) {
      return;
    }
    taskRunning = true;
    try {
      await main();
    } catch (err) {
      console.log(err);
    }
    taskRunning = false;
  },
  start: true,
  timeZone: "UTC",
});
